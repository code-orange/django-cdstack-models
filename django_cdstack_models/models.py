from datetime import datetime, timedelta
from django.utils import timezone

from django.conf import settings
from django.db import models
from djgeojson.fields import PointField

from django_mdat_customer.django_mdat_customer.models import MdatCustomers
from django_simple_notifier.django_simple_notifier.models import (
    SnotifierEmailContactZammadAbstract,
)


class CmdbChildgroups(models.Model):
    child_rel = models.ForeignKey("CmdbGroup", models.CASCADE, related_name="+")
    parent_rel = models.ForeignKey("CmdbGroup", models.CASCADE, related_name="+")
    link_created = models.DateTimeField(null=False, default=timezone.now)

    def __str__(self):
        return self.child_rel.name + " is part of " + self.parent_rel.name

    class Meta:
        db_table = "cmdb_childgroups"
        unique_together = (("child_rel", "parent_rel"),)


class CmdbGroup(models.Model):
    CONFIG_PKG_CHOICES = list()

    for module in settings.INSTALLED_APPS:
        module_name_short = module.split(".")[0]
        if module_name_short.startswith("django_cdstack_tpl_"):
            config_pkg_name = module_name_short[len("django_cdstack_tpl_") :]
            CONFIG_PKG_CHOICES.append((config_pkg_name, config_pkg_name))

    CONFIG_PKG_CHOICES.sort()

    inst_rel = models.ForeignKey("CmdbInstance", models.CASCADE)
    name = models.CharField(max_length=255)
    config_pkg = models.CharField(
        max_length=50, choices=CONFIG_PKG_CHOICES, default=None, blank=True, null=True
    )
    enabled = models.IntegerField()
    group_created = models.DateTimeField(null=False, default=timezone.now)

    def __str__(self):
        return self.inst_rel.name + " - " + self.name

    class Meta:
        db_table = "cmdb_group"
        unique_together = (("inst_rel", "name"),)


class CmdbOsFamily(models.Model):
    name = models.CharField(max_length=255, unique=True)
    os_family_added = models.DateTimeField(null=False, default=timezone.now)

    def __str__(self):
        return self.name

    @property
    def monitoring_id(self):
        monitoring_id = (
            str("os-" + self.name)
            .lower()
            .replace(" ", "-")
            .replace("/", "-")
            .replace("\\", "-")
        )
        monitoring_id = "".join(c for c in monitoring_id if c.isalnum() or c == "-")
        return monitoring_id

    class Meta:
        db_table = "cmdb_os_family"


class CmdbOsVersion(models.Model):
    name = models.CharField(max_length=255, unique=True)
    family = models.ForeignKey(CmdbOsFamily, models.CASCADE)
    agent_based = models.BooleanField(default=True)
    os_version_added = models.DateTimeField(null=False, default=timezone.now)
    deprecated = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    @property
    def monitoring_id(self):
        monitoring_id = (
            str("os-" + self.family.name + "-" + self.name)
            .lower()
            .replace("'", "")
            .replace('"', "")
            .strip()
            .replace(" ", "-")
            .replace("/", "-")
            .replace("\\", "-")
        )
        monitoring_id = "".join(c for c in monitoring_id if c.isalnum() or c == "-")
        return monitoring_id

    class Meta:
        db_table = "cmdb_os_version"


class CmdbHost(models.Model):
    inst_rel = models.ForeignKey("CmdbInstance", models.CASCADE)
    location_rel = models.ForeignKey(
        "CmdbLocation", models.CASCADE, null=True, blank=True
    )
    check_group_rel = models.ForeignKey(
        "CmdbGroup", models.CASCADE, null=True, blank=True
    )
    hostname = models.CharField(max_length=255)
    title = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    os = models.ForeignKey(CmdbOsVersion, models.DO_NOTHING, default=1)
    admin_comment = models.TextField(null=True, blank=True)
    enabled = models.IntegerField()
    geoloc = PointField()
    check_up_down = models.BooleanField(default=True)
    notify_enabled = models.BooleanField(default=True)
    notify_enabled_override = models.BooleanField(default=True)
    mon_skip_cmdb_prefix = models.BooleanField(default=False)
    mon_enable = models.BooleanField(default=True)
    mon_last_problem = models.DateTimeField(null=False, default=timezone.now)
    mon_last_ticketid = models.CharField(max_length=100, null=True, blank=True)
    cmdb_sync_enable = models.BooleanField(default=False)
    cmdb_sync_reboot_enable = models.BooleanField(default=False)
    pkg_sync_enable = models.BooleanField(default=False)
    unatt_upgr_enable = models.BooleanField(default=False)
    backup_enable = models.BooleanField(default=False)
    host_created = models.DateTimeField(null=False, default=timezone.now)
    host_last_fetch = models.DateTimeField(null=False, default=timezone.now)

    def __str__(self):
        return self.inst_rel.name + " - " + self.hostname

    def save(self, *args, **kwargs):
        # always enforce geolocation and default to instance default if unset
        if self.geoloc is None:
            self.geoloc = self.inst_rel.default_geoloc

        # fix host_last_fetch
        if self.host_last_fetch is None:
            self.host_last_fetch = timezone.now()

        super(CmdbHost, self).save(*args, **kwargs)

    @property
    def primary_group(self):
        try:
            primary_group = self.cmdbhostgroups_set.get(apply_order=0).group_rel
        except:
            pass
        else:
            return primary_group

        return False

    @property
    def is_online(self, timeout_min: int = 15):
        if (timezone.now() - timedelta(minutes=timeout_min)) < self.host_last_fetch:
            return True
        else:
            return False

    @property
    def universal_id(self):
        return str("h" + str(self.id)).lower()

    @property
    def monitoring_id(self):
        if self.mon_skip_cmdb_prefix:
            return self.hostname
        else:
            return self.universal_id

    @property
    def dependency_parent_hosts(self):
        hosts = list()

        for dependency in self.child_host.all():
            hosts.append(dependency.parent_host)

        if self.location_rel is not None:
            for dependency in self.location_rel.location.all():
                hosts.append(dependency.parent_host)

            if self.location_rel.location_master is not None:
                for (
                    dependency
                ) in self.location_rel.location_master.location_master.all():
                    hosts.append(dependency.parent_host)

        hosts = list(dict.fromkeys(hosts))

        try:
            hosts.remove(self.monitoring_id)
        except ValueError:
            pass

        return hosts

    @property
    def dependency_child_hosts(self):
        hosts = list()

        for dependency in self.parent_host.all():
            hosts.append(dependency.child_host)

        for dependency in CmdbDependencyLocationToHost.objects.filter(parent_host=self):
            hosts.append(dependency.child_host)

        for dependency in CmdbDependencyLocationMasterToHost.objects.filter(
            parent_host=self
        ):
            hosts.append(dependency.child_host)

        hosts = list(dict.fromkeys(hosts))

        try:
            hosts.remove(self.monitoring_id)
        except ValueError:
            pass

        return hosts

    class Meta:
        db_table = "cmdb_host"
        unique_together = (("inst_rel", "hostname"),)


class CmdbDependencyHostToHost(models.Model):
    child_host = models.ForeignKey(CmdbHost, models.CASCADE, related_name="child_host")
    parent_host = models.ForeignKey(
        CmdbHost, models.CASCADE, related_name="parent_host"
    )

    def __str__(self):
        return self.child_host.hostname + " depends on " + self.parent_host.hostname

    class Meta:
        db_table = "cmdb_depend_host2host"
        unique_together = (("child_host", "parent_host"),)


class CmdbHostgroups(models.Model):
    host_rel = models.ForeignKey(CmdbHost, models.CASCADE)
    group_rel = models.ForeignKey(CmdbGroup, models.CASCADE)
    apply_order = models.IntegerField()

    def __str__(self):
        return (
            self.host_rel.inst_rel.name
            + " - "
            + self.host_rel.hostname
            + " is part of "
            + self.group_rel.name
        )

    class Meta:
        db_table = "cmdb_hostgroups"
        unique_together = (
            ("host_rel", "group_rel"),
            ("host_rel", "apply_order"),
        )


class CmdbInstance(models.Model):
    name = models.CharField(max_length=30)
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING)
    default_geoloc = PointField()
    instance_created = models.DateTimeField(null=False, default=timezone.now)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "cmdb_instance"


class CmdbLocationMaster(models.Model):
    name = models.CharField(max_length=30)
    location_created = models.DateTimeField(null=False, default=timezone.now)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "cmdb_location_master"


class CmdbLocation(models.Model):
    name = models.CharField(max_length=30)
    inst_rel = models.ForeignKey(CmdbInstance, models.CASCADE)
    location_master = models.ForeignKey(
        CmdbLocationMaster, models.DO_NOTHING, null=True, blank=True
    )
    location_created = models.DateTimeField(null=False, default=timezone.now)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "cmdb_location"


class CmdbDependencyLocationMasterToHost(models.Model):
    location_master = models.ForeignKey(
        CmdbLocationMaster, models.CASCADE, related_name="location_master"
    )
    parent_host = models.ForeignKey(
        CmdbHost, models.CASCADE, related_name="loc_master_parent_host"
    )

    def __str__(self):
        return self.location_master.name + " depends on " + self.parent_host.hostname

    class Meta:
        db_table = "cmdb_depend_locmaster2host"
        unique_together = (("location_master", "parent_host"),)


class CmdbDependencyLocationToHost(models.Model):
    location = models.ForeignKey(CmdbLocation, models.CASCADE, related_name="location")
    parent_host = models.ForeignKey(
        CmdbHost, models.CASCADE, related_name="loc_parent_host"
    )

    def __str__(self):
        return self.location.name + " depends on " + self.parent_host.hostname

    class Meta:
        db_table = "cmdb_depend_loc2host"
        unique_together = (("location", "parent_host"),)


class CmdbVarsGlobalDefault(models.Model):
    name = models.CharField(max_length=60, unique=True)
    value = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "cmdb_vars_global_default"


class CmdbVarsInstance(models.Model):
    inst_rel = models.ForeignKey(CmdbInstance, models.CASCADE)
    name = models.CharField(max_length=60)
    value = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.inst_rel.name + " - " + self.name

    class Meta:
        db_table = "cmdb_vars_instance"
        unique_together = (("inst_rel", "name"),)


class CmdbVarsGroup(models.Model):
    group_rel = models.ForeignKey(CmdbGroup, models.CASCADE)
    name = models.CharField(max_length=60)
    value = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.group_rel.name + " - " + self.name

    class Meta:
        db_table = "cmdb_vars_group"
        unique_together = (("group_rel", "name"),)


class CmdbVarsLocation(models.Model):
    location_rel = models.ForeignKey(CmdbLocation, models.CASCADE)
    name = models.CharField(max_length=60)
    value = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.location_rel.name + " - " + self.name

    class Meta:
        db_table = "cmdb_vars_location"
        unique_together = (("location_rel", "name"),)


class CmdbVarsLocationMaster(models.Model):
    location_m_rel = models.ForeignKey(CmdbLocationMaster, models.CASCADE)
    name = models.CharField(max_length=60)
    value = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.location_m_rel.name + " - " + self.name

    class Meta:
        db_table = "cmdb_vars_location_m"
        unique_together = (("location_m_rel", "name"),)


class CmdbVarsHost(models.Model):
    host_rel = models.ForeignKey(CmdbHost, models.CASCADE)
    name = models.CharField(max_length=60)
    value = models.TextField(blank=True, null=True)

    def __str__(self):
        return (
            self.host_rel.inst_rel.name
            + " - "
            + self.host_rel.hostname
            + " - "
            + self.name
        )

    class Meta:
        db_table = "cmdb_vars_host"
        unique_together = (("host_rel", "name"),)


class CmdbMetaData(models.Model):
    inst_rel = models.ForeignKey("CmdbInstance", models.CASCADE)
    name = models.CharField(max_length=255)
    enabled = models.IntegerField()
    group_created = models.DateTimeField(null=False, default=timezone.now)

    def __str__(self):
        return self.inst_rel.name + " - " + self.name

    class Meta:
        db_table = "cmdb_meta_data"
        unique_together = (("inst_rel", "name"),)


class CmdbVarsMetaData(models.Model):
    md_rel = models.ForeignKey(CmdbMetaData, models.CASCADE)
    name = models.CharField(max_length=60)
    value = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.md_rel.name + " - " + self.name

    class Meta:
        db_table = "cmdb_vars_meta_data"
        unique_together = (("md_rel", "name"),)


class CmdbNotifierEmailInstance(SnotifierEmailContactZammadAbstract):
    instance = models.ForeignKey(CmdbInstance, models.CASCADE)

    class Meta:
        db_table = "cmdb_notifier_instance"


class CmdbNotifierEmailGroup(SnotifierEmailContactZammadAbstract):
    group = models.ForeignKey(CmdbGroup, models.CASCADE)

    class Meta:
        db_table = "cmdb_notifier_group"


class CmdbNotifierEmailHost(SnotifierEmailContactZammadAbstract):
    host = models.ForeignKey(CmdbHost, models.CASCADE)

    class Meta:
        db_table = "cmdb_notifier_host"


class CmdbAppData(models.Model):
    app_id = models.BigAutoField(primary_key=True)
    app_name = models.CharField(unique=True, max_length=250)

    class Meta:
        db_table = "cmdb_app_data"


class CmdbAppRel(models.Model):
    app_rel_id = models.BigAutoField(primary_key=True)
    host = models.ForeignKey(CmdbHost, models.CASCADE)
    app = models.ForeignKey(CmdbAppData, models.CASCADE)

    class Meta:
        db_table = "cmdb_app_rel"
        unique_together = (("host", "app"),)


class CmdbServiceData(models.Model):
    service_id = models.BigAutoField(primary_key=True)
    service_name = models.CharField(unique=True, max_length=250)

    class Meta:
        db_table = "cmdb_service_data"


class CmdbServiceRel(models.Model):
    service_rel_id = models.BigAutoField(primary_key=True)
    host = models.ForeignKey(CmdbHost, models.CASCADE)
    service = models.ForeignKey(CmdbServiceData, models.CASCADE)

    class Meta:
        db_table = "cmdb_service_rel"
        unique_together = (("host", "service"),)
