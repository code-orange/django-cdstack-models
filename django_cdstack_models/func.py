from django_cdstack_models.django_cdstack_models.models import *


def get_or_create_cmdb_host(hostname: str, instance: CmdbInstance):
    try:
        host = CmdbHost.objects.get(inst_rel=instance, hostname=hostname)
    except CmdbHost.DoesNotExist:
        host = CmdbHost(
            inst_rel=instance,
            hostname=hostname,
            enabled=1,
        )
        host.save(force_insert=True)

    return host
