from django.apps import apps
from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.db.models import ImageField

from . import models

try:
    from leaflet.admin import LeafletGeoAdmin
except ImportError:

    class LeafletGeoAdmin(ModelAdmin):
        pass


try:
    from django_cdstack_doc_images.django_cdstack_doc_images.models import CmdbHostImage
    from django_cdstack_doc_images.django_cdstack_doc_images.admin import (
        AdminImageWidget,
    )
except ImportError:
    doc_images_loaded = False
else:
    doc_images_loaded = True

for model in apps.get_app_config("django_cdstack_models").models.values():
    if model == models.CmdbHost:
        continue

    if model == models.CmdbHostgroups:
        continue

    if model == models.CmdbGroup:
        continue

    if model == models.CmdbInstance:
        continue

    if model == models.CmdbVarsInstance:
        continue

    if model == models.CmdbVarsGroup:
        continue

    if model == models.CmdbVarsHost:
        continue

    if model == models.CmdbNotifierEmailHost:
        continue

    if model == models.CmdbNotifierEmailGroup:
        continue

    if model == models.CmdbNotifierEmailInstance:
        continue

    admin.site.register(model)


# Hosts
class CmdbVarsHostInLine(admin.TabularInline):
    model = models.CmdbVarsHost
    extra = 0
    ordering = ["host_rel_id", "name"]


class CmdbNotifierEmailHostInLine(admin.TabularInline):
    model = models.CmdbNotifierEmailHost
    extra = 0


class CmdbHostgroupsInLine(admin.TabularInline):
    model = models.CmdbHostgroups
    extra = 0


if doc_images_loaded:

    class CmdbHostImageInLine(admin.TabularInline):
        model = CmdbHostImage
        extra = 0
        formfield_overrides = {ImageField: {"widget": AdminImageWidget}}


@admin.register(models.CmdbHost)
class CmdbHostAdmin(LeafletGeoAdmin):
    inlines = [CmdbHostgroupsInLine, CmdbNotifierEmailHostInLine, CmdbVarsHostInLine]

    def __init__(self, model, admin_site):
        if doc_images_loaded:
            self.inlines.insert(0, CmdbHostImageInLine)

        super().__init__(model, admin_site)


# Groups
class CmdbNotifierEmailGroupInLine(admin.TabularInline):
    model = models.CmdbNotifierEmailGroup
    extra = 0


class CmdbVarsGroupInLine(admin.TabularInline):
    model = models.CmdbVarsGroup
    extra = 0
    ordering = ["group_rel_id", "name"]


@admin.register(models.CmdbGroup)
class CmdbGroupAdmin(LeafletGeoAdmin):
    inlines = [CmdbNotifierEmailGroupInLine, CmdbVarsGroupInLine]


# Instance
class CmdbNotifierEmailInstanceInLine(admin.TabularInline):
    model = models.CmdbNotifierEmailInstance
    extra = 0


class CmdbVarsInstanceInLine(admin.TabularInline):
    model = models.CmdbVarsInstance
    extra = 0


@admin.register(models.CmdbInstance)
class CmdbInstanceAdmin(LeafletGeoAdmin):
    inlines = [CmdbNotifierEmailInstanceInLine, CmdbVarsInstanceInLine]
