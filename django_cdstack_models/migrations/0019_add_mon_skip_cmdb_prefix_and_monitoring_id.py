# Generated by Django 2.2.3 on 2019-07-08 18:17

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_cdstack_models", "0018_add_check_up_down_and_notify_enabled"),
    ]

    operations = [
        migrations.AddField(
            model_name="cmdbhost",
            name="mon_skip_cmdb_prefix",
            field=models.BooleanField(default=False),
        ),
    ]
