# Generated by Django 2.2.9 on 2020-02-03 15:03

from django.db import migrations, models


def migrate_config_pkg(apps, schema_editor):
    cmdb_group = apps.get_model("django_cdstack_models", "CmdbGroup")

    for cmdb_group_obj in cmdb_group.objects.all():
        cmdb_group_obj.config_pkg_new = cmdb_group_obj.config_pkg.name
        cmdb_group_obj.save()


class Migration(migrations.Migration):
    dependencies = [
        ("django_cdstack_models", "0032_add_location_variables"),
    ]

    operations = [
        migrations.AddField(
            model_name="cmdbgroup",
            name="config_pkg_new",
            field=models.CharField(blank=True, default=None, max_length=50, null=True),
        ),
        migrations.RunPython(migrate_config_pkg),
    ]
